/*
 * Copyright 2016-2022 www.mendmix.com.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.oneplatform.support.openapi;

import java.util.List;

import org.springframework.stereotype.Component;

import com.mendmix.gateway.model.OpenApiConfig;
import com.mendmix.gateway.security.OpenApiConfigProvider;

/**
 * @description <br>
 * @author <a href="mailto:vakinge@gmail.com">vakinge</a>
 * @date Nov 19, 2022
 */
@Component
public class PortalOpenApiConfigProvider implements OpenApiConfigProvider {

	@Override
	public List<OpenApiConfig> allOpenApiConfigs() {
		return null;
	}

	@Override
	public OpenApiConfig openApiConfig(String clientId) {
		return null;
	}

}
