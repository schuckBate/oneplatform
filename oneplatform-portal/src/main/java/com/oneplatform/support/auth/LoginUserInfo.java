/*
 * Copyright 2016-2018 www.mendmix.com.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.oneplatform.support.auth;

import com.mendmix.common.model.AuthUser;

/**
 * @description <br>
 * @author <a href="mailto:vakinge@gmail.com">vakin</a>
 * @date 2018年12月5日
 */
public class LoginUserInfo extends AuthUser {
	
	private String defaultTenantId;
	private String avatar;
	
	private String currentTenantId;
	private String currentSystemId;
	
	public String getDefaultTenantId() {
		return defaultTenantId;
	}
	public void setDefaultTenantId(String defaultTenantId) {
		this.defaultTenantId = defaultTenantId;
	}
	public String getAvatar() {
		return avatar;
	}
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	public String getCurrentTenantId() {
		return currentTenantId;
	}
	public void setCurrentTenantId(String currentTenantId) {
		this.currentTenantId = currentTenantId;
	}
	public String getCurrentSystemId() {
		return currentSystemId;
	}
	public void setCurrentSystemId(String currentSystemId) {
		this.currentSystemId = currentSystemId;
	}
	
	
}
