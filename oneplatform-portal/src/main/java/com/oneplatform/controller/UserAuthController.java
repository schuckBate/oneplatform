package com.oneplatform.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mendmix.common.CurrentRuntimeContext;
import com.mendmix.common.MendmixBaseException;
import com.mendmix.common.annotation.ApiMetadata;
import com.mendmix.common.constants.PermissionLevel;
import com.mendmix.common.model.AuthUser;
import com.mendmix.common.model.WrapperResponse;
import com.mendmix.gateway.api.AccountApi;
import com.mendmix.gateway.model.AccountScope;
import com.mendmix.security.SecurityDelegating;
import com.mendmix.security.model.UserSession;
import com.oneplatform.support.auth.LoginParam;
import com.oneplatform.support.auth.LoginUserInfo;

@Controller
public class UserAuthController {
	
	@Autowired
	private AccountApi accountApi;

	@RequestMapping(value = "login", method = RequestMethod.POST)
	@ApiMetadata(permissionLevel = PermissionLevel.Anonymous,actionLog = false)
    public @ResponseBody WrapperResponse<UserSession> doLogin(@RequestBody LoginParam param) {
		UserSession session = SecurityDelegating.doAuthentication(param.getAccount(), param.getPassword());
		//
		String systemId = CurrentRuntimeContext.getSystemId();
		if(systemId != null) {
			LoginUserInfo user = (LoginUserInfo) session.getUser();
			List<AccountScope> scopes = accountApi.findAccountScopes(user.getId());
			AccountScope scope = null;
			if(scopes != null) {
				scope = scopes.stream().filter(o -> systemId.equals(o.getSystemId())).findFirst().orElse(null);
			}
			if(scope == null) {
				throw new MendmixBaseException(403, "未授权登录(TYPE_NOT_MATCH)");
			}
			user.setDefaultTenantId(scope.getTenantId());
			user.setPrincipalId(scope.getPrincipalId());
			user.setAdmin(scope.isAdmin());	
		}
		return WrapperResponse.success(session);
	} 
	
	@RequestMapping(value = "logout", method = RequestMethod.POST)
	@ApiMetadata(permissionLevel = PermissionLevel.Anonymous,actionLog = false)
    public @ResponseBody WrapperResponse<Void> logout() {
		SecurityDelegating.doLogout();
		return WrapperResponse.success();
	} 
	
	@RequestMapping(value = "current_user", method = RequestMethod.GET)
	@ApiMetadata(permissionLevel = PermissionLevel.LoginRequired,actionLog = false)
    public @ResponseBody WrapperResponse<AuthUser> getCurrentUser() {
		UserSession session = SecurityDelegating.getAndValidateCurrentSession();
		return WrapperResponse.success(session.getUser());
	} 
}
