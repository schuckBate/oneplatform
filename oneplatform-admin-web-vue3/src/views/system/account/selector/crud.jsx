import { ref } from 'vue';
import * as api from '../api';
import { dict, compute } from '@fast-crud/fast-crud';
import { useMessage } from 'naive-ui';
export default function ({ expose }) {
  const pageRequest = async (query) => {
    return await api.GetList(query);
  };
  const message = useMessage();

  const selectedAccounts = ref([]);
  
  const onSelectionChange = (ids) => {
    let selectedRow = [];
    let tableRows = expose.getTableData();
    tableRows.map((row)=>{
      if(ids.includes(row.id)){
        selectedRow.push(row);
      }
    });
    selectedAccounts.value = selectedRow;
  };
  
  return {
    selectedAccounts,
    crudOptions: {
      request: {
        pageRequest,
      },
      search: {
        show: true,
      },
      actionbar: {
        show: false,
      },
      toolbar: {
        show: false,
      },
      rowHandle: {
        show: false,
      },
      table: {
        rowKey: (row) => row.id, //设置你的主键id获取方式， 默认(row)=>row.id
        'onUpdate:checkedRowKeys': onSelectionChange,
      },
      columns: {
        _checked: {
          title: '选择',
          form: { show: false },
          column: {
            type: 'selection',
            align: 'center',
            width: '55px',
            columnSetDisabled: true, //禁止在列设置中选择
          },
        },
        id: {
          title: 'ID',
          key: 'id',
          type: 'text',
          column: {
            show: false
          },
        },
        name: {
          title: '账号',
          key: 'name',
          type: 'text',
          search: { show: true },
          column: {
            width: 70,
          },
          form: {
            show: true,
          },
        },
        mobile: {
          title: '手机',
          key: 'mobile',
          type: 'text',
          column: {
            width: 80,
          },
        },
      },
    },
  };
}
