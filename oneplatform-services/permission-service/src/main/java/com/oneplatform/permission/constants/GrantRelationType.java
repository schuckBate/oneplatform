package com.oneplatform.system.constants;

import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * 
 * <br>
 * Class Name   : GrantRelationType
 *
 * @author jiangwei
 * @version 1.0.0
 * @date 2019年12月23日
 */
public enum GrantRelationType {
	apiToRole,funcToRole;
	
	@JsonCreator
	public static GrantRelationType getEnum(String key){
		for(GrantRelationType item : values()){
			if(item.name().equals(key)){
				return item;
			}
		}
		return null;
	}
}
