package com.oneplatform.system.dao.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import com.mendmix.mybatis.core.BaseMapper;
import com.oneplatform.system.dao.entity.ApiResourceEntity;
import com.oneplatform.system.dto.param.ApiResourceQueryParam;

public interface ApiResourceEntityMapper extends BaseMapper<ApiResourceEntity, Integer> {
	
	@Select("SELECT * FROM api_resource WHERE module_id=#{moduleId} AND uri=#{uri} AND http_method=#{method} LIMIT 1")
	@ResultMap("BaseResultMap")
	ApiResourceEntity findByRequestUri(@Param("moduleId") Integer moduleId,@Param("method") String method,@Param("uri") String uri);
    /**
     * 根据查询参数获取api列表(模糊查询）
     * @param queryParam
     * @return
     */
    List<ApiResourceEntity> findByQueryParam(ApiResourceQueryParam queryParam);

    /**
     * 根据id列表批量查询
     * @param ids id列表
     * @return
     */
    List<ApiResourceEntity> findByIds(List<Integer> ids);
    
    List<ApiResourceEntity> findByUserGrantRelations(@Param("userId") String userId);
    
    List<ApiResourceEntity> findByRolesGrantRelations(@Param("roleIds") List<String> roleIds);
    
    List<ApiResourceEntity> findByRolesGrantFunctions(@Param("roleIds") List<String> roleIds);
}