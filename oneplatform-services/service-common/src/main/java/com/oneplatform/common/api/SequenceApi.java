package com.oneplatform.common.api;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@FeignClient(value = "oneplatform-system-svc",contextId = "SequenceApi")
public interface SequenceApi {

    /**
     * 根据app和code获取一条序列号
     */
	@GetMapping("/sequence/get")
    @ResponseBody
    String getSequence(@RequestParam("code") String code);


}
