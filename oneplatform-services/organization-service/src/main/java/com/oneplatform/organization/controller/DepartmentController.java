package com.oneplatform.organization.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mendmix.common.annotation.ApiMetadata;
import com.mendmix.common.constants.PermissionLevel;
import com.mendmix.common.model.IdParam;
import com.mendmix.common.model.TreeModel;
import com.mendmix.common.util.BeanUtils;
import com.oneplatform.organization.dao.entity.DepartmentEntity;
import com.oneplatform.organization.dto.Department;
import com.oneplatform.organization.dto.param.DepartmentParam;
import com.oneplatform.organization.dto.param.DepartmentQueryParam;
import com.oneplatform.organization.service.DepartmentService;

@RestController
@RequestMapping("dept")
public class DepartmentController {

	private @Autowired DepartmentService departmentService;

	@ApiMetadata(permissionLevel = PermissionLevel.LoginRequired)
	@GetMapping("{id}")
	public Department getById(@PathVariable(value = "id") String id) {
		DepartmentEntity entity = departmentService.findById(id);
		return BeanUtils.copy(entity, Department.class);
	}

	@ApiMetadata(permissionLevel = PermissionLevel.PermissionRequired, actionLog = true)
	@PostMapping(value = "add")
	public IdParam<String> add(@RequestBody @Validated DepartmentParam param) {

		DepartmentEntity entity = BeanUtils.copy(param, DepartmentEntity.class);
		departmentService.addDepartment(entity);

		return new IdParam<>(entity.getId());
	}

	@ApiMetadata(permissionLevel = PermissionLevel.PermissionRequired, actionLog = true)
	@PostMapping(value = "update")
	public void update(@RequestBody @Validated DepartmentParam param) {
		DepartmentEntity entity = BeanUtils.copy(param, DepartmentEntity.class);
		departmentService.updateDepartment(entity);
	}
	
	@ApiMetadata(permissionLevel = PermissionLevel.PermissionRequired, actionLog = true)
	@PostMapping(value = "toggle")
	public void toggle(@RequestBody IdParam<String> param) {
		DepartmentEntity entity = departmentService.findById(param.getId());
		entity.setEnabled(!entity.getEnabled());
		departmentService.updateDepartment(entity);
	}

	@ApiMetadata(permissionLevel = PermissionLevel.PermissionRequired)
	@PostMapping(value = "delete")
	public void delete(@RequestBody IdParam<String> param) {
		DepartmentEntity entity = departmentService.findById(param.getId());
		entity.setDeleted(true);
		departmentService.updateDepartment(entity);
	}
	
	@ApiMetadata(permissionLevel = PermissionLevel.PermissionRequired, actionLog = false)
	@PostMapping(value = "list")
	public List<TreeModel> findDepartmentTree(@RequestBody DepartmentQueryParam param) {
		List<Department> departments = departmentService.findDepartmentList(param);
		return TreeModel.build(departments).getChildren();
	}
	
	@ApiMetadata(permissionLevel = PermissionLevel.LoginRequired, actionLog = false)
	@GetMapping(value = "tree")
	public List<TreeModel> findDepartmentTree() {
		final DepartmentQueryParam param = new DepartmentQueryParam();
		param.setEnabled(true);
		List<Department> departments = departmentService.findDepartmentList(param);
		return TreeModel.build(departments).getChildren();
	}
	
}
