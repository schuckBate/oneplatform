package com.oneplatform.organization.dao.mapper;

import java.util.List;

import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import com.mendmix.mybatis.core.BaseMapper;
import com.mendmix.mybatis.plugin.cache.annotation.Cache;
import com.oneplatform.organization.dao.entity.DepartmentEntity;
import com.oneplatform.organization.dto.param.DepartmentQueryParam;

public interface DepartmentEntityMapper extends BaseMapper<DepartmentEntity, String> {
	
	List<DepartmentEntity> findListByQueryParam(DepartmentQueryParam param);
	
	@Cache
	@Select("SELECT * FROM department WHERE (parent_id IS NULL OR parent_id = '') AND enabled=1 LIMIT 1")
	@ResultMap("BaseResultMap")
	DepartmentEntity findTopDepartment();
}