package com.oneplatform.system.dao.entity;

import javax.persistence.Column;
import javax.persistence.Table;

import com.oneplatform.system.dao.StandardBaseEntity;

@Table(name = "system_module")
public class BizSystemModuleEntity extends StandardBaseEntity {
 
    private String name;
    
    @Column(name = "system_id",updatable = false)
    private Integer systemId;
    
    @Column(name = "service_id")
    private String serviceId;
    
    @Column(name = "proxy_url")
    private String proxyUrl;
    
    @Column(name = "route_name")
    private String routeName;
    
    @Column(name = "route_rules")
    private String routeRules;

    @Column(name = "anonymous_uris")
    private String anonymousUris;
 
    public Integer getSystemId() {
		return systemId;
	}

	public void setSystemId(Integer systemId) {
		this.systemId = systemId;
	}
	
    /**
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }


	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}
	

	public String getProxyUrl() {
		return proxyUrl;
	}

	public void setProxyUrl(String proxyUrl) {
		this.proxyUrl = proxyUrl;
	}

	/**
     * @return route_name
     */
    public String getRouteName() {
        return routeName;
    }

    /**
     * @param routeName
     */
    public void setRouteName(String routeName) {
        this.routeName = routeName;
    }
    

    public String getRouteRules() {
		return routeRules;
	}

	public void setRouteRules(String routeRules) {
		this.routeRules = routeRules;
	}

	/**
     * @return anonymous_uris
     */
    public String getAnonymousUris() {
        return anonymousUris;
    }

    /**
     * @param anonymousUris
     */
    public void setAnonymousUris(String anonymousUris) {
        this.anonymousUris = anonymousUris;
    }

}