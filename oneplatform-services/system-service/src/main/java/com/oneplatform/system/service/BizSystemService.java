package com.oneplatform.system.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mendmix.common.model.Page;
import com.mendmix.common.model.PageParams;
import com.mendmix.common.util.BeanUtils;
import com.mendmix.mybatis.plugin.pagination.PageExecutor;
import com.mendmix.mybatis.plugin.pagination.PageExecutor.ConvertPageDataLoader;
import com.oneplatform.system.dao.entity.BizSystemModuleEntity;
import com.oneplatform.system.dao.mapper.BizSystemModuleEntityMapper;
import com.oneplatform.system.dto.BizSystemModule;

@Service
public class BizSystemService {

	@Autowired
	private BizSystemModuleEntityMapper systemModuleMapper;
	 
	public List<BizSystemModuleEntity> getSystemModules() {
		BizSystemModuleEntity example = new BizSystemModuleEntity();
		example.setEnabled(true);
		List<BizSystemModuleEntity> list = systemModuleMapper.selectByExample(example);
		return list;
	}
	
	public Integer addSystemModule(BizSystemModuleEntity entity) {
		systemModuleMapper.insertSelective(entity);
		return entity.getId();
	}
	
	public void updateSystemModule(BizSystemModuleEntity entity) {
		systemModuleMapper.updateByPrimaryKey(entity);
	}
	
	public BizSystemModuleEntity findSystemModuleById(Integer id) {
		return systemModuleMapper.selectByPrimaryKey(id);
	}
	
	public Page<BizSystemModule> pageQuerySystemModules(PageParams param){
		return PageExecutor.pagination(param, new ConvertPageDataLoader<BizSystemModuleEntity,BizSystemModule>() {

			@Override
			public List<BizSystemModuleEntity> load() {
				BizSystemModuleEntity example = new BizSystemModuleEntity();
				example.setEnabled(true);
				List<BizSystemModuleEntity> list = systemModuleMapper.selectByExample(example);
				return list;
			}

			@Override
			public BizSystemModule convert(BizSystemModuleEntity e) {
				return BeanUtils.copy(e, BizSystemModule.class);
			}
		});
	}

}
