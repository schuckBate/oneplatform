package com.oneplatform.system.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mendmix.common.CurrentRuntimeContext;
import com.mendmix.common.annotation.ApiMetadata;
import com.mendmix.common.constants.PermissionLevel;
import com.oneplatform.common.api.SequenceApi;
import com.oneplatform.system.service.SequenceGenService;

import io.swagger.annotations.ApiOperation;

/**
 * 
 * <br>
 * Class Name   : SequenceController
 *
 * @author jiangwei
 * @version 1.0.0
 * @date 2019年9月27日
 */
@RestController
public class SequenceController implements SequenceApi{

    private @Autowired SequenceGenService sequenceGenService;

	@ApiMetadata(permissionLevel = PermissionLevel.Anonymous)
	@ApiOperation(value = "生成序列号")
	@GetMapping("/sequence/get")
	public String getSequence(@RequestParam("code") String code) {
		Integer systemId = CurrentRuntimeContext.getSystemId(Integer.class);
        return sequenceGenService.genSequence(systemId,code);
	}

}
