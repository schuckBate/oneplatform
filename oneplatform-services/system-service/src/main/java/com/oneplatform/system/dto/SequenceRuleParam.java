package com.oneplatform.system.dto;

import javax.validation.constraints.NotBlank;

import io.swagger.annotations.ApiModelProperty;

/**
 * 
 * <br>
 * Class Name   : RegulationParam
 */
public class SequenceRuleParam {

    private Integer id;
    @ApiModelProperty("归属应用id")
    private Integer systemId;
	@ApiModelProperty("名称")
    private String name;
	@NotBlank(message = "序列业务编码不能为空")
	@ApiModelProperty("序列业务编码")
    private String code;
	@ApiModelProperty("前缀")
    private String prefix;
	@ApiModelProperty("时间序列表达式(如:{yyyy}{MM}{dd}{HH}{mm}{ss})")
    private String timeExpr;
	@ApiModelProperty("自增序列定长")
    private Integer seqLength;
	@ApiModelProperty(value="随机字符串类型",allowableValues="CHAR,NUMBER,ANY")
	private String randomType;
	@ApiModelProperty("随机字符串长度，0表示不需要随机字符串")
	private Integer randomLength = 0;
	@ApiModelProperty("初始自增序列值")
    private Integer firstSequence;
    @ApiModelProperty("备注")
    private String memo;
    
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	
	public Integer getSystemId() {
		return systemId;
	}
	
	public void setSystemId(Integer systemId) {
		this.systemId = systemId;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}
	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}
	/**
	 * @return the prefix
	 */
	public String getPrefix() {
		return prefix;
	}
	/**
	 * @param prefix the prefix to set
	 */
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
	/**
	 * @return the expression
	 */
	
	/**
	 * @return the seqLength
	 */
	public Integer getSeqLength() {
		return seqLength;
	}
	
	/**
	 * @return the randomType
	 */
	public String getRandomType() {
		return randomType;
	}
	/**
	 * @param randomType the randomType to set
	 */
	public void setRandomType(String randomType) {
		this.randomType = randomType;
	}
	/**
	 * @return the randomLength
	 */
	public Integer getRandomLength() {
		return randomLength;
	}
	/**
	 * @param randomLength the randomLength to set
	 */
	public void setRandomLength(Integer randomLength) {
		this.randomLength = randomLength;
	}
	/**
	 * @return the timeExpr
	 */
	public String getTimeExpr() {
		return timeExpr;
	}
	/**
	 * @param timeExpr the timeExpr to set
	 */
	public void setTimeExpr(String timeExpr) {
		this.timeExpr = timeExpr;
	}
	/**
	 * @param seqLength the seqLength to set
	 */
	public void setSeqLength(Integer seqLength) {
		this.seqLength = seqLength;
	}
	/**
	 * @return the firstSequence
	 */
	public Integer getFirstSequence() {
		return firstSequence;
	}
	/**
	 * @param firstSequence the firstSequence to set
	 */
	public void setFirstSequence(Integer firstSequence) {
		this.firstSequence = firstSequence;
	}

	/**
	 * @return the memo
	 */
	public String getMemo() {
		return memo;
	}
	/**
	 * @param memo the memo to set
	 */
	public void setMemo(String memo) {
		this.memo = memo;
	} 
}