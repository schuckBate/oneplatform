package com.oneplatform.system.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mendmix.common.MendmixBaseException;
import com.mendmix.common.model.IdParam;
import com.mendmix.common.util.AssertUtil;
import com.mendmix.common.util.BeanUtils;
import com.oneplatform.system.constants.BindingRelationType;
import com.oneplatform.system.dao.entity.FunctionResourceEntity;
import com.oneplatform.system.dao.mapper.FunctionResourceEntityMapper;
import com.oneplatform.system.dto.ResourceTreeModel;
import com.oneplatform.system.dto.param.FunctionResourceParam;
import com.oneplatform.system.dto.param.FunctionResourceQueryParam;


@Service
public class FunctionResourceService {

    @Autowired
    private FunctionResourceEntityMapper functionResourceMapper;
    @Autowired 
    private InternalRelationService relationService;
    
    /**
     * 新增菜单
     * @param param
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public IdParam<Integer> addFunctionResource(FunctionResourceParam param){

    	FunctionResourceEntity entity = BeanUtils.copy(param,FunctionResourceEntity.class);
    	entity.setIsDisplay(param.isDisplay());
    	FunctionResourceEntity parent = null;
    	if(param.getParentId() != null) {
    		parent = functionResourceMapper.selectByPrimaryKey(param.getParentId());
    		entity.setClientType(parent.getClientType());
    	}
    	
    	if(parent != null && entity.getCode() != null && !entity.getCode().contains("_")) {
    		entity.setCode(parent.getCode() + "_" + entity.getCode());
    	}
        
        functionResourceMapper.insertSelective(entity);
        
        if(param.getBindApiId() != null) {
        	List<String> sourceIds = Arrays.asList(param.getBindApiId().toString());
        	relationService.updateBindingRelations(BindingRelationType.apiToFunc, entity.getId().toString(), sourceIds);
        }

        return new IdParam<>(entity.getId());
    }

    /**
     * 根据id删除菜单
     * @param id
     */
    @Transactional(rollbackFor = Exception.class)
    public void deleteFunctionResource(Integer id){

        AssertUtil.notNull(id,"参数缺失[id]");
        FunctionResourceEntity entity = functionResourceMapper.selectByPrimaryKey(id);
        AssertUtil.notNull(entity,"删除菜单不存在");
        if(entity.getIsDefault()) {
        	throw new MendmixBaseException("默认菜单禁止删除");
        }
        entity.setEnabled(false);
        entity.setDeleted(true);
        functionResourceMapper.updateByPrimaryKeySelective(entity);
        //
        relationService.deleteFunctionResourceRelations(id);
    }

    /**
     * 更新菜单
     * @param param
     */
    @Transactional
    public void updateFunctionResource(FunctionResourceParam param){

        AssertUtil.notNull(param.getId(),"参数缺失[id]");
        FunctionResourceEntity oldEntity = functionResourceMapper.selectByPrimaryKey(param.getId());
        AssertUtil.notNull(oldEntity,"更新菜单不存在");

        FunctionResourceEntity entity = BeanUtils.copy(param,oldEntity);
        entity.setIsDisplay(param.isDisplay());
        functionResourceMapper.updateByPrimaryKeySelective(entity);
        
        if(param.getBindApiId() != null) {
        	List<String> sourceIds = Arrays.asList(param.getBindApiId().toString());
        	relationService.updateBindingRelations(BindingRelationType.apiToFunc, entity.getId().toString(), sourceIds);
        }
    }

    /**
     * 切换菜单激活状态
     * @param id
     */
    public void switchMenu(Integer id){

        AssertUtil.notNull(id,"参数缺失[id]");
        FunctionResourceEntity entity = functionResourceMapper.selectByPrimaryKey(id);

        entity.setEnabled(!entity.getEnabled());
        functionResourceMapper.updateByPrimaryKeySelective(entity);

    }

    /**
     * 根据id获取菜单
     * @param id
     * @return
     */
    public FunctionResourceEntity getFunctionResourceById(Integer id){
        AssertUtil.notNull(id,"参数缺失[id]");
        FunctionResourceEntity entity = functionResourceMapper.selectByPrimaryKey(id);
        return entity;
    }

    /**
     * 根据查询参数获取菜单列表
     * @param queryParam
     * @return
     */
    public List<FunctionResourceEntity> findByQueryParam(FunctionResourceQueryParam queryParam){
    	List<FunctionResourceEntity> list = functionResourceMapper.findByQueryParam(queryParam);
        return list;
    }

    public List<FunctionResourceEntity> findBySystemId(Integer systemId){
    	FunctionResourceQueryParam queryParam = new FunctionResourceQueryParam();
    	queryParam.setEnabled(true);
    	return functionResourceMapper.findByQueryParam(queryParam);
    }
    
    /**
     * 获取系统菜单树 
     * @param systemId
     * @param includeButton
     * @return
     */
    public List<ResourceTreeModel> getSystemMenuTree(boolean includeButton){
        // 1.查出系统的菜单列表
        FunctionResourceQueryParam queryParam = new FunctionResourceQueryParam();
        queryParam.setEnabled(true);
        List<FunctionResourceEntity> entities = functionResourceMapper.findByQueryParam(queryParam);
        if(entities.isEmpty()) {
            return new ArrayList<>(0);
        }
        
        List<ResourceTreeModel> items = entities.stream().map(o -> BeanUtils.copy(o, ResourceTreeModel.class)).collect(Collectors.toList());

        return items;
    }
    

}
