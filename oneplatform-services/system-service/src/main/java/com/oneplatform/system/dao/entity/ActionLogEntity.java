package com.oneplatform.system.dao.entity;

import com.mendmix.mybatis.core.BaseEntity;
import java.util.Date;
import javax.persistence.*;

@Table(name = "action_logs")
public class ActionLogEntity extends BaseEntity {
    @Id
    private String id;

    @Column(name = "log_type")
    private String logType;
    
    @Column(name = "system_id",updatable = false)
    private Integer systemId;
    
    @Column(name = "system_key")
    private String systemKey;

    @Column(name = "module_key")
    private String moduleKey;

    @Column(name = "action_name")
    private String actionName;

    @Column(name = "action_key")
    private String actionKey;

    @Column(name = "tenant_id")
    private String tenantId;

    @Column(name = "user_id")
    private String userId;

    @Column(name = "user_name")
    private String userName;

    @Column(name = "client_type")
    private String clientType;

    @Column(name = "client_ip")
    private String clientIp;

    @Column(name = "action_at")
    private Date actionAt;

    @Column(name = "biz_id")
    private String bizId;

    @Column(name = "trace_id")
    private String traceId;

    private Boolean successed;

    @Column(name = "use_time")
    private Integer useTime;

    @Column(name = "input_data")
    private String inputData;

    @Column(name = "output_data")
    private String outputData;

    private String exceptions;

    /**
     * @return id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }
    

    public String getLogType() {
		return logType;
	}

	public void setLogType(String logType) {
		this.logType = logType;
	}


    public Integer getSystemId() {
		return systemId;
	}

	public void setSystemId(Integer systemId) {
		this.systemId = systemId;
	}

	public String getSystemKey() {
		return systemKey;
	}

	public void setSystemKey(String systemKey) {
		this.systemKey = systemKey;
	}

	public String getModuleKey() {
		return moduleKey;
	}

	public void setModuleKey(String moduleKey) {
		this.moduleKey = moduleKey;
	}

	/**
     * @return action_name
     */
    public String getActionName() {
        return actionName;
    }

    /**
     * @param actionName
     */
    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

    /**
     * @return action_key
     */
    public String getActionKey() {
        return actionKey;
    }

    /**
     * @param actionKey
     */
    public void setActionKey(String actionKey) {
        this.actionKey = actionKey;
    }

    /**
     * @return tenant_id
     */
    public String getTenantId() {
        return tenantId;
    }

    /**
     * @param tenantId
     */
    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    /**
     * @return user_id
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * @return user_name
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * @return client_type
     */
    public String getClientType() {
        return clientType;
    }

    /**
     * @param clientType
     */
    public void setClientType(String clientType) {
        this.clientType = clientType;
    }

    /**
     * @return client_ip
     */
    public String getClientIp() {
        return clientIp;
    }

    /**
     * @param clientIp
     */
    public void setClientIp(String clientIp) {
        this.clientIp = clientIp;
    }

    public Date getActionAt() {
		return actionAt;
	}

	public void setActionAt(Date actionAt) {
		this.actionAt = actionAt;
	}

	/**
     * @return biz_id
     */
    public String getBizId() {
        return bizId;
    }

    /**
     * @param bizId
     */
    public void setBizId(String bizId) {
        this.bizId = bizId;
    }

    /**
     * @return trace_id
     */
    public String getTraceId() {
        return traceId;
    }

    /**
     * @param traceId
     */
    public void setTraceId(String traceId) {
        this.traceId = traceId;
    }

    /**
     * @return successed
     */
    public Boolean getSuccessed() {
        return successed;
    }

    /**
     * @param successed
     */
    public void setSuccessed(Boolean successed) {
        this.successed = successed;
    }

    /**
     * @return use_time
     */
    public Integer getUseTime() {
        return useTime;
    }

    /**
     * @param useTime
     */
    public void setUseTime(Integer useTime) {
        this.useTime = useTime;
    }

    /**
     * @return input_data
     */
    public String getInputData() {
        return inputData;
    }

    /**
     * @param inputData
     */
    public void setInputData(String inputData) {
        this.inputData = inputData;
    }

    /**
     * @return output_data
     */
    public String getOutputData() {
        return outputData;
    }

    /**
     * @param outputData
     */
    public void setOutputData(String outputData) {
        this.outputData = outputData;
    }

    /**
     * @return exceptions
     */
    public String getExceptions() {
        return exceptions;
    }

    /**
     * @param exceptions
     */
    public void setExceptions(String exceptions) {
        this.exceptions = exceptions;
    }
}