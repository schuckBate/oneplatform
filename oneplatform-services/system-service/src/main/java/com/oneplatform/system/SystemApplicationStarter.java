package com.oneplatform.system;

import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.quartz.QuartzAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.mendmix.springcloud.starter.BaseApplicationStarter;

/**
 * 
 * 
 * @description <br>
 * @author <a href="mailto:vakinge@gmail.com">jiangwei</a>
 * @date May 22, 2022
 */
@SpringBootApplication(exclude = {QuartzAutoConfiguration.class})
@EnableDiscoveryClient
@EnableTransactionManagement
@ComponentScan(value = {"com.oneplatform","com.mendmix.springcloud.autoconfigure"})
public class SystemApplicationStarter extends BaseApplicationStarter{

	public static void main(String[] args) {
		long starTime = before(args);
		new SpringApplicationBuilder(SystemApplicationStarter.class).web(WebApplicationType.SERVLET).run(args);
		after(starTime);
	}
	
}
