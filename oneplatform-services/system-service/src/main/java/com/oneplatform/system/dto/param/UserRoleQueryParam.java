package com.oneplatform.system.dto.param;

public class UserRoleQueryParam  {

    /**
     * 用户组名称
     */
    private String name;
    
    private String code;
    
    private String roleType;

    /**
     * 关联部门id
     */
    private String departmentId;

    /**
     * 是否系统默认
     */
    private Boolean isDefault;

    /**
     * 激活状态
     */
    private Boolean enabled;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getRoleType() {
		return roleType;
	}

	public void setRoleType(String roleType) {
		this.roleType = roleType;
	}

	public String getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(String departmentId) {
        this.departmentId = departmentId;
    }

    public Boolean getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(Boolean aDefault) {
        isDefault = aDefault;
    }


    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

}