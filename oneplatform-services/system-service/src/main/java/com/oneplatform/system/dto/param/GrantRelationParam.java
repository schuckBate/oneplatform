package com.oneplatform.system.dto.param;

import java.util.List;

import javax.validation.constraints.NotNull;

import com.oneplatform.system.constants.GrantRelationType;

public class GrantRelationParam {

    /**
     * 授权资源类型
     */
    @NotNull(message = "授权资源类型不能为空")
    private GrantRelationType relationType;

    /**
     * 资源ID
     */
    private List<String> sourceIdList;
    /**
     * 目标ID
     */
    @NotNull(message = "目标id不能为空")
    private String targetId;
    
	public GrantRelationType getRelationType() {
		return relationType;
	}
	public void setRelationType(GrantRelationType relationType) {
		this.relationType = relationType;
	}
	public List<String> getSourceIdList() {
		return sourceIdList;
	}
	public void setSourceIdList(List<String> sourceIdList) {
		this.sourceIdList = sourceIdList;
	}
	public String getTargetId() {
		return targetId;
	}
	public void setTargetId(String targetId) {
		this.targetId = targetId;
	}
    
    

}