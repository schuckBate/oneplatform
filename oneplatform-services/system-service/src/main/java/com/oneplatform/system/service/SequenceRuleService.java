package com.oneplatform.system.service;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mendmix.common.MendmixBaseException;
import com.mendmix.common.model.Page;
import com.mendmix.common.model.PageParams;
import com.mendmix.common.util.BeanUtils;
import com.mendmix.mybatis.plugin.pagination.PageExecutor;
import com.mendmix.mybatis.plugin.pagination.PageExecutor.ConvertPageDataLoader;
import com.oneplatform.system.constants.SeqTimeExpr;
import com.oneplatform.system.dao.entity.SequenceRuleEntity;
import com.oneplatform.system.dao.mapper.SequenceRuleEntityMapper;
import com.oneplatform.system.dto.SequenceRule;
import com.oneplatform.system.dto.SequenceRuleParam;

/**
 * 
 * <br>
 * Class Name   : RuleService
 */
@Service
public class SequenceRuleService {
	

    private static Pattern timeExprPattern = Pattern.compile("(\\{).*?(?=\\})");
    private static List<String> timeExprList = new ArrayList<String>();
    static{
    	for (SeqTimeExpr seqTimeExpr : SeqTimeExpr.values()) {
    		timeExprList.add(seqTimeExpr.getExpr());
		}
    }
    
	private @Autowired SequenceRuleEntityMapper mapper;
	
	

	public Integer addRule(SequenceRuleParam param) {
		validateTimeExpr(param.getTimeExpr());
		//验证code是否存在
		if(mapper.findByCode(param.getSystemId(),param.getCode()) != null){
			throw new MendmixBaseException("编码["+param.getCode()+"]已存在");
		}
		SequenceRuleEntity entity = BeanUtils.copy(param, SequenceRuleEntity.class);
	
		mapper.insertSelective(entity);
		
		return entity.getId();
	}

	public void updateRule(SequenceRuleParam param) {
		validateTimeExpr(param.getTimeExpr());
		SequenceRuleEntity entity = mapper.selectByPrimaryKey(param.getId());
		mapper.updateByPrimaryKeySelective(entity);
	}

	
	public SequenceRuleEntity findRule(Integer id){
		return mapper.selectByPrimaryKey(id);
	}
	
	public void enableSwitch(Integer id){
		SequenceRuleEntity entity = mapper.selectByPrimaryKey(id);
		entity.setEnabled(!entity.getEnabled());
		mapper.updateByPrimaryKey(entity);
	}

	public Page<SequenceRule> pageQuery(PageParams pageParams) {

		return PageExecutor.pagination(pageParams, new ConvertPageDataLoader<SequenceRuleEntity,SequenceRule>() {
			@Override
			public List<SequenceRuleEntity> load() {
				return mapper.selectAll();
			}

			@Override
			public SequenceRule convert(SequenceRuleEntity e) {
				return BeanUtils.copy(e, SequenceRule.class);
			}
		});

	}
	
	private void validateTimeExpr(String timeExpr){
		if(StringUtils.isBlank(timeExpr))return;
		Matcher matcher = timeExprPattern.matcher(timeExpr);
		String expr;
		while(matcher.find()){
			expr = matcher.group() + "}";
			if(!timeExprList.contains(expr))throw new MendmixBaseException("时间表达式"+expr+"不支持");
		}
	}
}
