package com.oneplatform.system.task;

import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import com.mendmix.common2.lock.redis.RedisDistributeLock;
import com.mendmix.scheduler.AbstractJob;
import com.mendmix.scheduler.JobContext;
import com.mendmix.scheduler.annotation.ScheduleConf;
import com.oneplatform.system.constants.SeqTimeExpr;
import com.oneplatform.system.dao.entity.SequenceRuleEntity;
import com.oneplatform.system.dao.mapper.SequenceRuleEntityMapper;

/**
 * 自增序列重置定时任务
 */
@Component
@ScheduleConf(cronExpr="0 59 23 * * ?")
public class AutoIncrSeqResetTask extends AbstractJob{
	
	private final static Logger logger = LoggerFactory.getLogger("com.oneplatform.system.task");

	@Autowired
	private SequenceRuleEntityMapper mapper;
	@Autowired
	private TransactionTemplate transactionTemplate;
	
	@Override
	public void doJob(JobContext context) throws Exception {
		SequenceRuleEntity example = new SequenceRuleEntity();
		example.setEnabled(true);
		List<SequenceRuleEntity> rules = mapper.selectByExample(example);
		
		Calendar calendar = Calendar.getInstance();
		RedisDistributeLock lock;
		for (SequenceRuleEntity rule : rules) {
			if(StringUtils.isBlank(rule.getTimeExpr()))continue;
			//按月
			if(rule.getTimeExpr().endsWith(SeqTimeExpr.MONTH.getExpr())) {
				if(calendar.get(Calendar.DAY_OF_MONTH) != calendar.getActualMaximum(Calendar.DAY_OF_MONTH)) {
					continue;
				}
			}else if(rule.getTimeExpr().endsWith(SeqTimeExpr.YEAR.getExpr())) {//按年
				if(calendar.get(Calendar.DAY_OF_YEAR) != calendar.getActualMaximum(Calendar.DAY_OF_YEAR)) {
					continue;
				}
			}
			lock = new RedisDistributeLock(rule.getCode());
			if(lock.tryLock()) {//未获取到锁说明其他节点在处理
				try {
					logger.info(">> 开始编码【{}】序列重置...",rule.getCode());
					updateLastSequenceValue(rule);
					logger.info(">> 结束编码【{}】序列重置...",rule.getCode());
				} finally {
					lock.unlock();
				}
			}
		}
	}

	/**
	 * @param rule
	 */
	private void updateLastSequenceValue(SequenceRuleEntity rule) {
		transactionTemplate.execute(new TransactionCallbackWithoutResult() {
			@Override
			protected void doInTransactionWithoutResult(TransactionStatus status) {
				SequenceRuleEntity entity = mapper.getByIdForUpdate(rule.getId());
				mapper.updateLastSequenceById(entity.getId(), entity.getFirstSequence());
			}
		});
		
	}

	@Override
	public boolean parallelEnabled() {
		return false;
	}

}
