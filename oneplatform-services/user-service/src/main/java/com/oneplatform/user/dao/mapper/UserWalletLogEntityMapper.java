package com.oneplatform.user.dao.mapper;

import com.mendmix.mybatis.core.BaseMapper;
import com.oneplatform.user.dao.entity.UserWalletLogEntity;

public interface UserWalletLogEntityMapper extends BaseMapper<UserWalletLogEntity, String> {
}