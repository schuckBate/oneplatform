import config from "@/config"
import http from "@/utils/request"

export default {
	menu: {
		list: {
			url: `${config.API_URL}/sys/menu/list`,
			name: "获取菜单",
			get: async function() {
				return await http.get(this.url);
			}
		}
	},
	dic: {
		tree: {
			url: `${config.API_URL}/system/dic/tree`,
			name: "获取字典树",
			get: async function() {
				return await http.get(this.url);
			}
		},
		list: {
			url: `${config.API_URL}/system/dic/list`,
			name: "字典明细",
			get: async function(params) {
				return await http.get(this.url, params);
			}
		},
		get: {
			url: `${config.API_URL}/system/dic/get`,
			name: "获取字典数据",
			get: async function(params) {
				return await http.get(this.url, params);
			}
		}
	},
	role: {
		list: {
			url: `${config.API_URL}/system/role/list2`,
			name: "获取角色列表",
			get: async function(params) {
				return await http.get(this.url, params);
			}
		}
	},
	dept: {
		list: {
			url: `${config.API_URL}/org/dept/tree`,
			name: "获取部门列表",
			get: async function(params) {
				return await http.get(this.url, params);
			}
		}
	},
	user: {
		list: {
			url: `${config.API_URL}/system/user/list`,
			name: "获取用户列表",
			get: async function(params) {
				return await http.get(this.url, params);
			}
		}
	},
	app: {
		list: {
			url: `${config.API_URL}/system/app/list`,
			name: "应用列表",
			get: async function() {
				return await http.get(this.url);
			}
		}
	},
	log: {
		list: {
			url: `${config.API_URL}/system/log/list`,
			name: "日志列表",
			get: async function(params) {
				return await http.get(this.url, params);
			}
		}
	},
	table: {
		list: {
			url: `${config.API_URL}/system/table/list`,
			name: "表格列管理列表",
			get: async function(params) {
				return await http.get(this.url, params);
			}
		},
		info: {
			url: `${config.API_URL}/system/table/info`,
			name: "表格列管理详情",
			get: async function(params) {
				return await http.get(this.url, params);
			}
		}
	},
	tasks: {
		list: {
			url: `${config.API_URL}/user/task/list`,
			name: "系统任务管理",
			get: async function(params) {
				return await http.post(this.url, params);
			}
		}
	}
}

function convertToMenuItems(items) {
	let menus = [];
	items.map((item) => {
		let menu = {};
		menu.name = item.code;
		menu.component = item.specAttrs.component;
		menu.path = item.specAttrs.path;
		menu.meta = {
			title: item.name,
			"type": "menu",
			hidden: !item.isDisplay,
			icon: item.specAttrs.icon
		};
		if (item.children) {
			menu.children = convertToMenuItems(item.children);
		}
		menus.push(menu);
	});

	return menus;
}
