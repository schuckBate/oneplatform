## 项目介绍
`oneplatform`是一个企业级中后台解决方案，后端基于Springcloud 2.x与mendmix构建，前端基于naiveui+fastCrud构建。

## 🚀🚀知识星球🚀🚀
>欢迎加入我的知识星球。提供mendmix各种问题交流，定期分享架构实践、架构案例、面试技巧等。

<img src="https://www.jeesuite.com/assets/images/ads/zsxq-002.png" width="290" height="465" />

